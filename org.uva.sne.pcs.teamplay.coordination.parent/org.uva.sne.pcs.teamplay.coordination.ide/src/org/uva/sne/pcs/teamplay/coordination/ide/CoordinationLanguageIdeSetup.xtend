/*
 * generated by Xtext 2.24.0
 */
package org.uva.sne.pcs.teamplay.coordination.ide

import com.google.inject.Guice
import org.eclipse.xtext.util.Modules2
import org.uva.sne.pcs.teamplay.coordination.CoordinationLanguageRuntimeModule
import org.uva.sne.pcs.teamplay.coordination.CoordinationLanguageStandaloneSetup

/**
 * Initialization support for running Xtext languages as language servers.
 */
class CoordinationLanguageIdeSetup extends CoordinationLanguageStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new CoordinationLanguageRuntimeModule, new CoordinationLanguageIdeModule))
	}
	
}
