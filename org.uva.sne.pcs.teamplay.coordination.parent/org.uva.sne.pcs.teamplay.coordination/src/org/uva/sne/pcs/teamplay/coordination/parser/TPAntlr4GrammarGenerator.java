package org.uva.sne.pcs.teamplay.coordination.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.AbstractRule;
import org.eclipse.xtext.Alternatives;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.CrossReference;
import org.eclipse.xtext.Grammar;
import org.eclipse.xtext.Group;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.TerminalRule;
import org.eclipse.xtext.xtext.FlattenedGrammarAccess;
import org.eclipse.xtext.xtext.RuleFilter;
import org.eclipse.xtext.xtext.RuleNames;
import org.eclipse.xtext.xtext.generator.model.IXtextGeneratorFileSystemAccess;
import org.eclipse.xtext.xtext.generator.parser.antlr.AbstractAntlrGrammarGenerator;
import org.eclipse.xtext.xtext.generator.parser.antlr.AntlrOptions;
import org.eclipse.xtext.xtext.generator.parser.antlr.GrammarNaming;
import org.eclipse.xtext.xtext.generator.parser.antlr.KeywordHelper;
import org.eclipse.xtext.xtext.generator.util.SyntheticTerminalDetector;

import com.google.inject.Inject;

public class TPAntlr4GrammarGenerator extends AbstractAntlrGrammarGenerator {
   
   List<String> knownStructs = new ArrayList<>();
   Map<AbstractElement, String> visitedAlternatives = new HashMap<>();

   @Inject TPAntlr4GrammarNaming naming;
   
   @Inject
   SyntheticTerminalDetector std;
   
   @Override
   public void generate(Grammar it, AntlrOptions options, IXtextGeneratorFileSystemAccess fsa) {
       keywordHelper = KeywordHelper.getHelper(it);
       RuleFilter filter = new RuleFilter();
       filter.setDiscardUnreachableRules(options.isSkipUnusedRules());
       RuleNames ruleNames = RuleNames.getRuleNames(it, true);
       Grammar flattened = new FlattenedGrammarAccess(ruleNames, filter).getFlattenedGrammar();
       
       fsa.generateFile(getGrammarNaming().getParserGrammar(it).getGrammarFileName(), compileParser(flattened, options));
   }

   @Override
   protected GrammarNaming getGrammarNaming() {
       return naming;
   }
   
   @Override
   protected String compileParserOptions(Grammar it, AntlrOptions options) {
       return "";
   }

	@Override
	protected String compileParserHeader(Grammar it, AntlrOptions options) {
		return "@parser::postinclude {\n" + 
				"#include <cstdlib>\n" + 
				"#include <cstdio>\n" + 
				"#include <string>\n" + 
				"#include <vector>\n" +
				"}\n"
				;
	}

	@Override
	protected String compileParserMembers(Grammar it, AntlrOptions options) {
		// TODO Auto-generated method stub
		
		String structs = "";
		for(AbstractRule r : it.getRules()) {
			if(! (r instanceof ParserRule)) continue;
			
			//String fields = memberDataTypeEbnf(r.getAlternatives());
			Map<String, String> field_datatype = new HashMap<>();
			gatherMemberFields(r.getAlternatives(), field_datatype);
			
			knownStructs.add(r.getName());
			structs += "struct "+r.getName()+"_s {\n" ;
			for(String el : field_datatype.keySet()) {
				structs += "\t"+field_datatype.get(el)+" "+el;
				if(field_datatype.get(el).equals("bool"))
					structs += " = false";
				else if(field_datatype.get(el).equals("string"))
					structs += " = \"\"";
				structs += ";\n";
			}
			structs += "};\n";
		}
		String _return = "@parser::members {\n";
		for(String s : knownStructs)
			_return += "struct "+s+";\n";
		_return += structs;
		_return += "}\n";
		return _return;
	}
	private void gatherMemberFields(AbstractElement it, Map<String, String> knownFields) {
		
		if(it instanceof Assignment) {
			Assignment ass = (Assignment) it;
			AbstractElement el = ass.getTerminal();
			if((el instanceof RuleCall && ((RuleCall) el).getRule() instanceof TerminalRule) || 
					el instanceof Keyword || el instanceof CrossReference || el instanceof Alternatives) {
				if(knownFields.containsKey(ass.getFeature())) {
					if(!knownFields.get(ass.getFeature()).contains("vector")) {
						String type = knownFields.get(ass.getFeature());
						if(type.equals("string"))
							type = "std::"+type;
						knownFields.put(ass.getFeature(), "std::vector<"+type+">");
					}
				}
				else if(ass.getOperator().equals("?=")) {
					knownFields.put(ass.getFeature(), "bool"); 
				}
				else if(ass.getOperator().equals("=")) {
					knownFields.put(ass.getFeature(), "std::string");
				}
				else
					knownFields.put(ass.getFeature(), "std::vector<std::string>");
			}
		}
		else if(it instanceof Group) {
			for(AbstractElement el : ((Group)it).getElements()) {
				gatherMemberFields(el, knownFields);
			}
		}
		else if(it instanceof Alternatives) {
			for(AbstractElement el : ((Alternatives)it).getElements()) {
				gatherMemberFields(el, knownFields);
			}
		}
	}

	@Override
	 protected String compileLexerHeader(Grammar it, AntlrOptions options) {
		return "";
	}
	
	@Override
	protected String compileInit(AbstractRule it, AntlrOptions options) {
		return knownStructs.contains(it.getName()) ? " returns ["+it.getName()+"_s current]" : "";
	}
	String test = "";
	@Override
	protected String assignmentEbnf(AbstractElement it, Assignment assignment, AntlrOptions options,
			boolean supportActions) {
		String ebnf = super.assignmentEbnf(it, assignment, options, supportActions);
		String _return = ebnf;
		AbstractElement el = assignment.getTerminal();
		if(el instanceof Alternatives) {
			if(!visitedAlternatives.containsKey(el)) {
				Alternatives alt = (Alternatives) el;
				_return = "("+"\n";
				for(AbstractElement t : alt.getElements()) {
					RuleCall c = (RuleCall) t;
					_return += c.getRule().getName();
					if(assignment.getOperator().equals("="))
						 _return += " {$current."+assignment.getFeature()+" = $"+c.getRule().getName()+".text;}";
					 else
						 _return += " {$current."+assignment.getFeature()+".push_back($"+c.getRule().getName()+".text);}";
					_return += "\n|";
				}
				_return = _return.substring(0, _return.length()-1);
				_return += ")";
				visitedAlternatives.put(el, _return);
			}
			else
				return visitedAlternatives.get(el);
		}
		else if((el instanceof RuleCall && ((RuleCall) el).getRule() instanceof TerminalRule) || 
				el instanceof CrossReference ) {
			 if(assignment.getOperator().equals("="))
				 _return += " {$current."+assignment.getFeature()+" = $"+ebnf+".text;}";
			 else
				 _return += " {$current."+assignment.getFeature()+".push_back($"+ebnf+".text);}";
		}
		else if(assignment.getOperator().equals("?=") && el instanceof Keyword)
			_return += " {$current."+assignment.getFeature()+" = true ;}";
		
		return _return;
	}
	
	//@Override
	/*protected String ebnf(AbstractElement it, AntlrOptions options, boolean supportActions) {
		String _return = "";
		if(mustBeParenthesized(it)) {
			_return = "(";
			_return += ebnfPredicate(it, options);
			_return += ebnf2(it, options, supportActions);
			_return += ")";
		}
		else {
			_return = ebnf2(it, options, supportActions);
		}
		if(it.getCardinality() != null) {
			if(_return.charAt(_return.length()-1) == '\n')
				_return = _return.substring(0, _return.length()-1);
			_return += it.getCardinality()+" ";
		}
		return _return;
	}*/
	
	@Override
	protected boolean _mustBeParenthesized(Assignment it) {
		
		if(super._mustBeParenthesized(it))
			return true;
		return (it.getCardinality() != null);
	}
	
	@Override
	protected CharSequence _compileRule(TerminalRule it, Grammar grammar, AntlrOptions options) {
		if(it.getName().equals("RULE_ML_COMMENT"))
			return "RULE_ML_COMMENT: '/*' .*? '*/' -> skip;";
		if(it.getName().equals("RULE_SL_COMMENT"))
			return "RULE_SL_COMMENT: ('//' ~('\\n'|'\\r')* '\\r'? '\\n') -> skip;";
		
		return super._compileRule(it, grammar, options);
	}
}
