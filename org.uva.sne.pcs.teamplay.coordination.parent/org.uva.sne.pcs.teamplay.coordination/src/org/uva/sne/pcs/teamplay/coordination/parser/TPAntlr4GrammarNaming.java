package org.uva.sne.pcs.teamplay.coordination.parser;

import org.eclipse.xtext.Grammar;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.xtext.generator.model.TypeReference;
import org.eclipse.xtext.xtext.generator.parser.antlr.AntlrGrammar;
import org.eclipse.xtext.xtext.generator.parser.antlr.GrammarNaming;

public class TPAntlr4GrammarNaming extends GrammarNaming {
   @Override 
   public TypeReference getInternalParserSuperClass(Grammar it) {
       return null;
   }
   
   @Override 
   public boolean isCombinedGrammar(Grammar it) {
       return true;
   }
   
   @Override 
   protected String getGrammarNamePrefix(Grammar it) {
       return "";
   }
   
   @Override
    public AntlrGrammar getParserGrammar(Grammar it) {
       return new TPAntlr4Grammar(getInternalParserPackage(it), 
               getGrammarNamePrefix(it)+GrammarUtil.getSimpleName(it)+ ((!isCombinedGrammar(it)) ? "Parser" : ""));
    }
}
