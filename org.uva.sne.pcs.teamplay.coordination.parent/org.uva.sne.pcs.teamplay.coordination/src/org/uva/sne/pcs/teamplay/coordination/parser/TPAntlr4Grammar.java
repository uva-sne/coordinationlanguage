package org.uva.sne.pcs.teamplay.coordination.parser;

import org.eclipse.xtext.xtext.generator.parser.antlr.AntlrGrammar;

public class TPAntlr4Grammar extends AntlrGrammar {
	
	public TPAntlr4Grammar(String packageName, String simpleName) {
		super(packageName, simpleName);
	}

	@Override
	public String getGrammarFileName() {
		return getName().replace('.', '/') + ".g4";
	}
	@Override
	public String getName() {
		return getSimpleName();
	}
}
