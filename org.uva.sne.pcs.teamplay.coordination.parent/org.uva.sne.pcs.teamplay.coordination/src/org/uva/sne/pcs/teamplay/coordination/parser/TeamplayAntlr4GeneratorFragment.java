package org.uva.sne.pcs.teamplay.coordination.parser;

import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.AbstractRule;
import org.eclipse.xtext.CompoundElement;
import org.eclipse.xtext.xtext.generator.grammarAccess.GrammarAccessExtensions;
import org.eclipse.xtext.xtext.generator.model.FileAccessFactory;
import org.eclipse.xtext.xtext.generator.model.IXtextGeneratorFileSystemAccess;
import org.eclipse.xtext.xtext.generator.parser.antlr.AbstractAntlrGeneratorFragment2;
import org.eclipse.xtext.xtext.generator.parser.antlr.AntlrContentAssistGrammarGenerator;
import org.eclipse.xtext.xtext.generator.parser.antlr.AntlrDebugGrammarGenerator;
import org.eclipse.xtext.xtext.generator.parser.antlr.AntlrGrammar;
import org.eclipse.xtext.xtext.generator.parser.antlr.CombinedGrammarMarker;
import org.eclipse.xtext.xtext.generator.parser.antlr.ContentAssistGrammarNaming;
import org.eclipse.xtext.xtext.generator.parser.antlr.GrammarNaming;
import org.eclipse.xtext.xtext.generator.parser.antlr.KeywordHelper;

import com.google.inject.Inject;

public class TeamplayAntlr4GeneratorFragment extends AbstractAntlrGeneratorFragment2 {
	
	@Inject TPAntlr4GrammarGenerator productionGenerator;
	@Inject AntlrContentAssistGrammarGenerator contentAssistGenerator;
	@Inject AntlrDebugGrammarGenerator debugGenerator;

	@Inject FileAccessFactory fileFactory;

	@Inject GrammarNaming productionNaming;
	@Inject ContentAssistGrammarNaming contentAssistNaming;
	
	@Inject GrammarAccessExtensions grammarUtil;
	
	public boolean removeBacktrackingGuards;
	int lookaheadThreshold;
	public boolean partialParsing;
	
	@Override
	protected void doGenerate() {
		new KeywordHelper(getGrammar(), getOptions().isIgnoreCase(), grammarUtil);
		CombinedGrammarMarker combinedGram = new CombinedGrammarMarker(false);
		combinedGram.attachToEmfObject(getGrammar());
		generateProductionGrammar();
		combinedGram.removeFromEmfObject(getGrammar());
	}

	private void generateProductionGrammar() {
		GrammarNaming naming = productionNaming;
		IXtextGeneratorFileSystemAccess fsa = getProjectConfig().getRuntime().getSrcGen();
		
		productionGenerator.generate(getGrammar(), getOptions(), fsa);
	}
}
